In order to install the ST-Link utilities on Linux (Ubuntu) we have to make sure to have the libusb-dev library installed. On Ubuntu systems you can install the necessary library by executing:

*sudo apt-get install libusb-1.0-0-dev*

We will now download, build and install the latest ST-Link utilities from scratch

git clone https://github.com/texane/stlink stlink.git
cd stlink
make
#install binaries:
sudo cp build/Debug/st-* /usr/local/bin
#install udev rules
sudo cp etc/udev/rules.d/49-stlinkv* /etc/udev/rules.d/
#and restart udev
sudo udevadm control --reload
